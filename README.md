# Bloc Tempo

Small arcade game made on unity.

## The Game

The goal is to move from bloc to bloc by pressing the correct inputs. Each bloc has an input linked (up, right, down, left) and an interaction type (tap, hold, double-tap).
There are 3 difficulties that will change the amount of time you have to react at each movement (1 to 3 seconds). Each bloc successfully passed will increase your score depending on the difficulty you chose.

The game ends if : 
- You move on a red bloc
- You run out of time

The game is only playable using a keyboard (arrows).

In-game screenshots (12/10/2020) :

![Start](https://i.imgur.com/58xUeVy.png)
![In Game](https://i.imgur.com/gufccuZ.png)
![Game Over](https://i.imgur.com/3p4d1UO.png)

I don't plan to add anything more, for now. I didn't add any music/sounds because I'm not skilled enough in this field and because I feel that the game should be played with some music of your choice in background.

The build can be found at : [Itch.io game page](https://corentinpb.itch.io/bloc-tempo).