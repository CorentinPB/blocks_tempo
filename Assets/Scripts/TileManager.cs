﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class used to handle all the tile modifications (spawn/despawn) and game status (playing/stopped/paused/ended).
public class TileManager : MonoBehaviour
{
    private bool endGameOverTimer;
    private float currentGameOverTimerValue;
    private float gameOverTimerValue;
    // We store the current game score.
    private int currentScore;
    // Game difficulty variable, will be used later when difficulty will be implemented.
    private int gameDifficulty;
    // Number of objects in the tile pooling system. 
    private int numberOfTilePool = 73;
    // All input sprites organized in an accessible way.
    private List<List<Sprite>> inputSpriteList;
    // Temporary list used to store tiles during the input random generation.
    private List<GameObject> adjacentTileListTemporary;
    // Contains all adjacent green tile (updated at each every player movement).
    private List<GameObject> adjacentGreenTileList;
    // Contains all adjacent red tile (updated at each every player movement).
    private List<GameObject> adjacentRedTileList;
    // Queue used as a pooling system for all the tiles in the game.
    private Queue<GameObject> tilePool;
    // Red custom color.
    private Color redColor;
    // Green custom color.
    private Color greenColor;
    // Int variable that indicates the current game status.
    private GameStatus currentGameStatus;

    // All input sprites references.
    [SerializeField] private List<Sprite> inputUpSpriteList;
    [SerializeField] private List<Sprite> inputRightSpriteList;
    [SerializeField] private List<Sprite> inputDownSpriteList;
    [SerializeField] private List<Sprite> inputLeftSpriteList;
    // Reference to the UI Manager.
    [SerializeField] private UIManager uiManager;
    // Reference to the tile prefab.
    [SerializeField] private GameObject tilePrefab;
    // Reference to the playerController script. 
    [SerializeField] private PlayerController playerController;

    // Variables used to keep track of the next coordinate where the tile lines will be spawned.
    private int currentPositiveXSpawn = 8;
    private int currentNegativeXSpawn = -8;
    private int currentPositiveYSpawn = 4;
    private int currentNegativeYSpawn = -4;

    // Enum variable for the game status.
    public enum GameStatus { SETTINGS, PLAY, END, PAUSE };

    private void Start() {
        // Basic initialization
        currentGameStatus = GameStatus.SETTINGS;
        gameDifficulty = 2;
        gameOverTimerValue = 2f;
        endGameOverTimer = true;
        adjacentRedTileList = new List<GameObject>();
        adjacentGreenTileList = new List<GameObject>();
        adjacentTileListTemporary = new List<GameObject>();

        // Setting up custom colors.
        redColor = new Color(0.5f, 0f, 0f);
        greenColor = new Color(0f, 0.5f, 0f);
        
        // Setting up the game for the first start.
        InitializeObjectPooler();
        InitializeInputSpriteList();
        InitializeBaseTiles();
        RandomizeTileColors();
        SetupPlayerAdjacentTiles(0);
    }

    private void Update() {
        if (!endGameOverTimer) {
            if (currentGameOverTimerValue < 0) {
                EndGame();
                endGameOverTimer = true;
            } else {
                currentGameOverTimerValue -= Time.deltaTime;
                uiManager.UpdateTimerValue(System.Math.Round(currentGameOverTimerValue, 1));
            }
        }
    }

    // Used to initialize the tile pool.
    private void InitializeObjectPooler() {
        tilePool = new Queue<GameObject>();
        for (int i = 0; i < numberOfTilePool; i++) {
            GameObject tile = Instantiate(tilePrefab, Vector3.zero, Quaternion.identity);
            tilePool.Enqueue(tile);
            tile.SetActive(false);
        }
    }

    private void InitializeInputSpriteList() {
        inputSpriteList = new List<List<Sprite>>();

        for (int i = 0; i < 4; i++) {
            inputSpriteList.Add(new List<Sprite>());
            for (int j = 0; j < 3; j++) {
                switch(i) {
                    case 0:
                        inputSpriteList[i].Add(inputUpSpriteList[j]);
                        break;
                    case 1:
                        inputSpriteList[i].Add(inputRightSpriteList[j]);
                        break;
                    case 2:
                        inputSpriteList[i].Add(inputDownSpriteList[j]);
                        break;
                    case 3:
                        inputSpriteList[i].Add(inputLeftSpriteList[j]);
                        break;
                }
            }
        }
    }

    // Used to get a tile from the pool and placing it on the position in parameter.
    public GameObject GetNewTile(Vector3 newTilePosition) {
        GameObject newTile = tilePool.Dequeue();
        newTile.transform.position = newTilePosition;
        newTile.SetActive(true);
        return newTile;
    }

    // Used to return a tile to the pool.
    public void ReturnTile(GameObject tile) {
        tilePool.Enqueue(tile);
        tile.SetActive(false);
    }

    // Used to initialize all initial tiles.
    private void InitializeBaseTiles() {
        for (int i = -8; i <= 8; i+=2) {
            for (int j = -4; j <= 4; j+=2) {
                GameObject newTile = GetNewTile(new Vector3(i, j, 0f));
                newTile.transform.parent = this.transform;
            }
        }
    }

    // Spawns a line of tile on the left of the screen.
    public void SpawnLineLeft() {
        // Updating new coordinates to spawn tiles.
        currentNegativeXSpawn -= 2;
        currentPositiveXSpawn -= 2;
        for (int i = currentNegativeYSpawn; i <= currentPositiveYSpawn; i+=2) {
            // Getting az new tile from the spawner.
            GameObject newTile = GetNewTile(new Vector3(currentNegativeXSpawn, i, 0f));
            Tile tileScript = newTile.GetComponent<Tile>();
            // Setting up the tile colors if the color is white.
            if (tileScript.GetCurrentColor() == Color.white) {
                tileScript.SetCurrentColor(Random.Range(0,2));
            }
            // Setting up the tile spawn timer.
            tileScript.ResetSpawnTimer();
            // Re-parenting the new tile. 
            newTile.transform.parent = this.transform;
        }
    }

    // Spawns a line of tile on the right of the screen.
    public void SpawnLineRight() {
        currentPositiveXSpawn += 2;
        currentNegativeXSpawn += 2;
        for (int i = currentNegativeYSpawn; i <= currentPositiveYSpawn; i+=2) {
            GameObject newTile = GetNewTile(new Vector3(currentPositiveXSpawn, i, 0f));
            Tile tileScript = newTile.GetComponent<Tile>();
            // Setting up the tile colors if the color is white.
            if (tileScript.GetCurrentColor() == Color.white) {
                tileScript.SetCurrentColor(Random.Range(0,2));
            }
            tileScript.ResetSpawnTimer();
            newTile.transform.parent = this.transform;
        }
    }

    // Spawns a line of tile on the top of the screen.
    public void SpawnLineUp() {
        currentPositiveYSpawn += 2;
        currentNegativeYSpawn += 2;
        for (int i = currentNegativeXSpawn; i <= currentPositiveXSpawn; i+=2) {
            GameObject newTile = GetNewTile(new Vector3(i, currentPositiveYSpawn, 0f));
            Tile tileScript = newTile.GetComponent<Tile>();
            // Setting up the tile colors if the color is white.
            if (tileScript.GetCurrentColor() == Color.white) {
                tileScript.SetCurrentColor(Random.Range(0,2));
            }
            tileScript.ResetSpawnTimer();
            newTile.transform.parent = this.transform;
        }
    }

    // Spawns a line of tile on the bottom of the screen.
    public void SpawnLineDown() {
        currentNegativeYSpawn -= 2;
        currentPositiveYSpawn -= 2;
        for (int i = currentNegativeXSpawn; i <= currentPositiveXSpawn; i+=2) {
            GameObject newTile = GetNewTile(new Vector3(i, currentNegativeYSpawn, 0f));
            Tile tileScript = newTile.GetComponent<Tile>();
            // Setting up the tile colors if the color is white.
            if (tileScript.GetCurrentColor() == Color.white) {
                tileScript.SetCurrentColor(Random.Range(0,2));
            }
            tileScript.ResetSpawnTimer();
            newTile.transform.parent = this.transform;
        }
    }

    // Not really efficient, iterate through all tiles in the game and randomize the colors.
    private void RandomizeTileColors() {
        foreach(Transform tile in transform) {
            Tile tileScript = tile.GetComponent<Tile>();
            tileScript.SetCurrentColor(Random.Range(0,2));
        }
    }

    // Not efficient as well, used to setup the tiles that are adjcacent to the player.
    private void SetupPlayerAdjacentTiles(int typeOfSetup) {
        Vector2 playerPosition = playerController.GetPlayerPosition();
        switch (typeOfSetup) {
            case 0:
                // We search for all adjacent tiles to set their color to red.
                foreach(Transform tile in transform) {
                    Tile tileScript = tile.GetComponent<Tile>();
                    if (IsTileAdjacentToPlayerPos(tile.transform.position, playerPosition)) {
                        tileScript.SetCurrentColor(0);
                    }
                }
                break;
            case 1:
                // We search for all adjacent tiles and check if they have at least a green color.
                // We also store all adjacent tiles in a list to set their input sprites later.
                bool greenTilePresent = false;
                
                adjacentRedTileList.Clear();
                adjacentGreenTileList.Clear();

                foreach(Transform tile in transform) {
                    if (IsTileAdjacentToPlayerPos(tile.transform.position, playerPosition)) {
                        Tile tileScript = tile.GetComponent<Tile>();
                        if (tileScript.GetCurrentColor() == greenColor) {
                            greenTilePresent = true;
                            adjacentGreenTileList.Add(tile.gameObject);
                        }
                        else {
                            adjacentRedTileList.Add(tile.gameObject);
                        }
                    }
                }

                // If there isn't any green color in adjacent tiles, we randomize 1 to 4 tiles to set to green.
                if (!greenTilePresent) {
                    int numberOfGreenColor = Random.Range(1, 5);
                    for (int i = 0; i <= numberOfGreenColor - 1; i++) {
                        int randomIndex = Random.Range(0, adjacentRedTileList.Count);
                        adjacentRedTileList[randomIndex].GetComponent<Tile>().SetCurrentColor(1);
                        // We don't forget to update the adjacent tile list.
                        adjacentGreenTileList.Add(adjacentRedTileList[randomIndex]);
                        adjacentRedTileList.Remove(adjacentRedTileList[randomIndex]);
                    }
                }

                // Creating the random inputs types for every adjacent tiles

                // We first fill up the temporary tile list (that will be emptied along the process) 
                adjacentTileListTemporary.Clear();
                foreach (GameObject greenTile in adjacentGreenTileList) {
                    adjacentTileListTemporary.Add(greenTile);
                }
                foreach (GameObject redTile in adjacentRedTileList) {
                    adjacentTileListTemporary.Add(redTile);
                }

                // We randomize the number of special tiles.
                int randomNumberOfSpecialInput = Random.Range(0, adjacentTileListTemporary.Count);
                int remainingNumberOfSpecialInputs = randomNumberOfSpecialInput;

                // We add special tiles as long as we have adjacent tilees and special inputs remaining.
                while (adjacentTileListTemporary.Count > 0 && remainingNumberOfSpecialInputs > 0) {
                    
                    // Inverted tiles requires 2 tiles so if there's not enough tiles, we exclude it from the random selection of special input type.
                    int specialInputTile;
                    if (adjacentTileListTemporary.Count >= 2) {
                        specialInputTile = Random.Range(0, 3);
                    } else {
                        specialInputTile = Random.Range(0, 2);
                    }
                    
                    // Special input adding process.
                    switch (specialInputTile) {
                        // Double tap special input.
                        case 0:
                            int firstRandomIndex = Random.Range(0, adjacentTileListTemporary.Count);
                            int firstTileDirection = GetDirectionOfAdjacentTile(playerPosition, adjacentTileListTemporary[firstRandomIndex].transform.position);

                            Tile firstTileScript = adjacentTileListTemporary[firstRandomIndex].GetComponent<Tile>();

                            firstTileScript.SetCurrentInput(inputSpriteList[firstTileDirection][specialInputTile + 1], firstTileDirection, 1);
                            adjacentTileListTemporary.Remove(adjacentTileListTemporary[firstRandomIndex]);

                            firstTileScript.StartInputScaleTween();

                            remainingNumberOfSpecialInputs--;
                            break;
                        // Hold special input.
                        case 1:
                            int secondRandomIndex = Random.Range(0, adjacentTileListTemporary.Count);
                            int secondTileDirection = GetDirectionOfAdjacentTile(playerPosition, adjacentTileListTemporary[secondRandomIndex].transform.position);

                            Tile secondTileScript = adjacentTileListTemporary[secondRandomIndex].GetComponent<Tile>();

                            secondTileScript.SetCurrentInput(inputSpriteList[GetDirectionOfAdjacentTile(playerPosition, adjacentTileListTemporary[secondRandomIndex].transform.position)][specialInputTile + 1], secondTileDirection, 2);
                            adjacentTileListTemporary.Remove(adjacentTileListTemporary[secondRandomIndex]);

                            secondTileScript.StartInputScaleTween();

                            remainingNumberOfSpecialInputs--;
                            break;
                        // Inverted special input.
                        case 2:
                            int thirdRandomIndex = Random.Range(0, adjacentTileListTemporary.Count);
                            GameObject temporaryTile = adjacentTileListTemporary[thirdRandomIndex];
                            adjacentTileListTemporary.Remove(adjacentTileListTemporary[thirdRandomIndex]);
                            int fourthRandomIndex = Random.Range(0, adjacentTileListTemporary.Count);

                            int thirdTileDirection = GetDirectionOfAdjacentTile(playerPosition, temporaryTile.transform.position);
                            int fourthTileDirection = GetDirectionOfAdjacentTile(playerPosition, adjacentTileListTemporary[fourthRandomIndex].transform.position);

                            Tile thirdTileScript = temporaryTile.GetComponent<Tile>();
                            Tile fourthTileScript = adjacentTileListTemporary[fourthRandomIndex].GetComponent<Tile>();

                            fourthTileScript.SetCurrentInput(inputSpriteList[GetDirectionOfAdjacentTile(playerPosition, temporaryTile.transform.position)][0], fourthTileDirection, 3, thirdTileDirection);
                            thirdTileScript.SetCurrentInput(inputSpriteList[GetDirectionOfAdjacentTile(playerPosition, adjacentTileListTemporary[fourthRandomIndex].transform.position)][0], thirdTileDirection, 3, fourthTileDirection);

                            adjacentTileListTemporary.Remove(adjacentTileListTemporary[fourthRandomIndex]);

                            thirdTileScript.GetComponent<Tile>().StartInputScaleTween();
                            fourthTileScript.GetComponent<Tile>().StartInputScaleTween();

                            remainingNumberOfSpecialInputs--;
                            break;
                    }
                }

                // If there's still adjacent tiles who hasn't received any inputs, we set default inputs on them.
                foreach(GameObject adjacentTile in adjacentGreenTileList) {
                    Tile adjacentTileScript = adjacentTile.GetComponent<Tile>();
                    
                    int tileDirection = GetDirectionOfAdjacentTile(playerPosition, adjacentTile.transform.position);

                    if (!adjacentTileScript.HasAnInput()) {
                        adjacentTileScript.SetCurrentInput(inputSpriteList[GetDirectionOfAdjacentTile(playerPosition, adjacentTile.transform.position)][0], tileDirection, 0);
                        adjacentTileScript.StartInputScaleTween();
                    }
                }
                foreach(GameObject adjacentTile in adjacentRedTileList) {
                    Tile adjacentTileScript = adjacentTile.GetComponent<Tile>();
                    
                    int tileDirection = GetDirectionOfAdjacentTile(playerPosition, adjacentTile.transform.position);
                    
                    if (!adjacentTileScript.HasAnInput()) {
                        adjacentTileScript.SetCurrentInput(inputSpriteList[GetDirectionOfAdjacentTile(playerPosition, adjacentTile.transform.position)][0], tileDirection, 0);
                        adjacentTileScript.StartInputScaleTween();
                    }
                }
                StartTimer();
                break;
        }
    }

    // Fonction that returns the direction of the tile in parameter (tilePosition) from the playerPosition also in parameter.
    // 0 : Up, 1 : Right, 2 : Down, 3 : Left.
    private int GetDirectionOfAdjacentTile(Vector2 playerPosition, Vector2 tilePosition) {
        if (tilePosition.x == playerPosition.x && tilePosition.y - 2 == playerPosition.y) {
            return 0;
        } else if (tilePosition.x - 2 == playerPosition.x && tilePosition.y == playerPosition.y) {
            return 1;
        } else if (tilePosition.x == playerPosition.x && tilePosition.y + 2 == playerPosition.y) {
            return 2;
        } else if (tilePosition.x + 2 == playerPosition.x && tilePosition.y == playerPosition.y) {
            return 3;
        } else {
            // It will create an error in the sprite list that can be easily tracked.
            return -1;
        }
    }

    private void StartTimer() {
        currentGameOverTimerValue = gameOverTimerValue;
        endGameOverTimer = false;
    }

    private void CheckPlayerTileColor() {
        Vector2 playerPosition = playerController.GetPlayerPosition();
        foreach(Transform tile in transform) {
            if (tile.transform.position.x == playerPosition.x && tile.transform.position.y == playerPosition.y) {
                Tile tileScript = tile.GetComponent<Tile>();
                if (tileScript.GetCurrentColor() == redColor) {
                    EndGame();
                }
                break;
            }
        }  
    }

    public int GetInputType(ref int returnedDirection) {
        foreach(GameObject adjacentTile in adjacentGreenTileList) {
            Tile adjacentTileScript = adjacentTile.GetComponent<Tile>();

            if (adjacentTileScript.GetInputDirection() == returnedDirection) {
                int originalInputDirection = adjacentTileScript.GetOriginalInputDirection();
                if (originalInputDirection != -1)
                    returnedDirection = originalInputDirection;
                return adjacentTileScript.GetInputType();
            }
        }
        foreach(GameObject adjacentTile in adjacentRedTileList) {
            Tile adjacentTileScript = adjacentTile.GetComponent<Tile>();

            if (adjacentTileScript.GetInputDirection() == returnedDirection) {
                int originalInputDirection = adjacentTileScript.GetOriginalInputDirection();
                if (originalInputDirection != -1)
                    returnedDirection = originalInputDirection;
                return adjacentTileScript.GetInputType();
            }
        }
        return -1;
    }

    // Method used to pause all tweens from the adjacent tiles.
    private void PauseAdjacentTilesTweens() {
        foreach(GameObject adjacentTile in adjacentGreenTileList) {
            Tile adjacentTileScript = adjacentTile.GetComponent<Tile>();

            adjacentTileScript.PauseTween();
        }
        foreach(GameObject adjacentTile in adjacentRedTileList) {
            Tile adjacentTileScript = adjacentTile.GetComponent<Tile>();

            adjacentTileScript.PauseTween();
        }
    }

    // Method used to resume all tweens from the adjacent tiles.
    private void ResumeAdjacentTilesTweens() {
        foreach(GameObject adjacentTile in adjacentGreenTileList) {
            Tile adjacentTileScript = adjacentTile.GetComponent<Tile>();

            adjacentTileScript.ResumeTween();
        }
        foreach(GameObject adjacentTile in adjacentRedTileList) {
            Tile adjacentTileScript = adjacentTile.GetComponent<Tile>();

            adjacentTileScript.ResumeTween();
        }
    }

    // Used to change the player's tile Color to white, needs to be changed so we don't have to iterate through all tiles.
    private void ChangePlayerTileColor() {
        Vector2 playerPosition = playerController.GetPlayerPosition();
        foreach(Transform tile in transform) {
            if (tile.transform.position.x == playerPosition.x && tile.transform.position.y == playerPosition.y) {
                Tile tileScript = tile.GetComponent<Tile>();
                tileScript.SetCurrentColor(2);
                break;
            }
        }
    }

    // We randomize all tiles colors, setup the tiles adjacent to the players and changing the player's tile color.
    public void NextPlayerMovement() {
        CheckPlayerTileColor();
        if (currentGameStatus == GameStatus.PLAY) {
            RandomizeTileColors();
            SetupPlayerAdjacentTiles(1);
            ChangePlayerTileColor();
        }
    }

    // When the player starts to move, we make the inputs sprites disappear.
    public void StartingPlayerMovement(int inputTypeSelected) {
        foreach(GameObject adjacentTile in adjacentGreenTileList) {
            adjacentTile.GetComponent<Tile>().StartInputTransparencyTween();
        }
        foreach(GameObject adjacentTile in adjacentRedTileList) {
            adjacentTile.GetComponent<Tile>().StartInputTransparencyTween();
        }

        switch (inputTypeSelected) {
            case 0:
                currentScore += gameDifficulty;
                break;
            case 1:
                currentScore += (gameDifficulty * 3);
                break;
            case 2:
                currentScore += (gameDifficulty * 2);
                break;
            case 3:
                currentScore += (gameDifficulty * 2);
                break;
        }

        endGameOverTimer = true;
    }

    // Used to check if the tile in parameter is adjacent to the player's tile in parameter as well.
    private bool IsTileAdjacentToPlayerPos(Vector2 tilePosition, Vector2 playerPosition) {
        return (tilePosition.x + 2 == playerPosition.x && tilePosition.y == playerPosition.y ||
            tilePosition.x - 2 == playerPosition.x && tilePosition.y == playerPosition.y ||
            tilePosition.x == playerPosition.x && tilePosition.y + 2 == playerPosition.y ||
            tilePosition.x == playerPosition.x && tilePosition.y - 2 == playerPosition.y);
    }

    public GameStatus GetGameStatus() {
        return currentGameStatus;
    }

    // Start a new game.
    public void StartGame() {
        Debug.Log("STARTING GAME");
        currentGameStatus = GameStatus.PLAY;
        SetupPlayerAdjacentTiles(1);
        ChangePlayerTileColor();
        currentScore = 0;
        playerController.StartAnimator();
    }

    // End the current game.
    public void EndGame() {
        Debug.Log("ENDING GAME");
        RandomizeTileColors();
        SetupPlayerAdjacentTiles(0);
        currentGameStatus = GameStatus.END;
        uiManager.DisplayEndMenu(currentScore);
        playerController.StopAnimator();
    }

    // Pause the game.
    public void PauseGame() {
        Debug.Log("PAUSING GAME");
        PauseAdjacentTilesTweens();
        currentGameStatus = GameStatus.PAUSE;
        endGameOverTimer = true;
        playerController.PauseAnimator();
        playerController.PauseTween();
    }

    // Set the start status.
    public void SetStartStatus() {
        Debug.Log("SETTING UP GAME");
        currentGameStatus = GameStatus.SETTINGS;
    }

    // Resume the paused game.
    public void ResumeGame() {
        Debug.Log("RESUMING GAME");
        ResumeAdjacentTilesTweens();
        currentGameStatus = GameStatus.PLAY;
        endGameOverTimer = false;
        playerController.ResumeAnimator();
        playerController.ResumeTween();
    }

    // Set the game difficulty.
    public void SetDifficulty(int newDifficulty) {
        gameDifficulty = newDifficulty;
        switch (gameDifficulty) {
            case 1:
                gameOverTimerValue = 3f;
                break;
            case 2:
                gameOverTimerValue = 2f;
                break;
            case 3:
                gameOverTimerValue = 1f;
                break;
        }
    }
}
