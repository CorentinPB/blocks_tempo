﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Tween;
using TMPro;

// Class used to handle all UI interfaces and inputs (except player's movements and inputs in game).
public class UIManager : MonoBehaviour
{
    // Time for a countdown step.
    private float countDownTime = 0.75f;
    // Time for an input difficulty step.
    private float inputsDifficultyTime = 0.05f;
    // Int used to keep track of the countdown steps.
    private int countDownStatus;
    // Bool used to switch from resume button to quit button.
    private bool pauseMenuIndex;
    // Bool used to switch from restart button to settings button.
    private bool endMenuIndex;
    // We need this bool to prevent the player from moving from one button to another when he already pressed a button.
    private bool inPauseKeyPressing;
    // Same goes for the end menu.
    private bool inEndKeyPressing;
    // We need this bool to prevent any inputs during countdown.
    private bool inCountdown;
    // Bool used to switch from right to left input selected in the settings menu.
    private bool rightDifficultyInputPressed;


    // Tween used to scale the inputs difficulty values.
    private Tween<float> inputsDifficultyScaleTween;
    // Tween used to scale the countdown values.
    private Tween<float> countdownScaleTween;
    // Tween function for the difficulty inputs scale.
    private System.Action<Tween<float>> updateDifficultyInputsScale;
    private System.Action<Tween<float>> updateDifficultyInputsScaleEnd;
    // Tween function for the countdown scale.
    private System.Action<Tween<float>> updateCountDownScale;
    // Tween function to update the countdown sprite once one part of the countdown is finished.
    private System.Action<Tween<float>> updateCountDownSprite;

    // Reference to the score text.
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private TMP_Text timerText;

    // References to all menus sprites required at runtime. 
    [SerializeField] private Sprite difficultyEasy;
    [SerializeField] private Sprite difficultyMedium;
    [SerializeField] private Sprite difficultyHard;
    [SerializeField] private Sprite unpressedButton;
    [SerializeField] private Sprite pressedButton;
    [SerializeField] private Sprite selectedButton;
    [SerializeField] private Sprite num3Sprite;
    [SerializeField] private Sprite num2Sprite;
    [SerializeField] private Sprite num1Sprite;

    // References to all Image used in menus that needs to be modified at runtime. 
    [SerializeField] private Image difficultyImage;
    [SerializeField] private Image pauseResumeButton;
    [SerializeField] private Image pauseQuitButton;
    [SerializeField] private Image endRestartButton;
    [SerializeField] private Image endSettingsButton;
    [SerializeField] private Image countDownImage;
    [SerializeField] private Image leftDifficultyInputImage;
    [SerializeField] private Image rightDifficultyInputImage;

    // References to all menus gameobjects. 
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject endMenu;
    [SerializeField] private GameObject settingsMenu;
    [SerializeField] private GameObject countDown;

    // Reference to the tileManager script. 
    [SerializeField] private TileManager tileManager;

    private void Start() {
        inCountdown = false;

        inputsDifficultyScaleTween = FloatTween.Create("TweenInputsDifficultydownScale", 1f, 1f, 0.001f, EasingFunction.Ease.EaseInOutCubic, updateDifficultyInputsScale);

        updateCountDownScale = (t) =>
        {
            countDown.transform.localScale = new Vector3(t.Value, t.Value, t.Value);
        };

        updateDifficultyInputsScale = (t) =>
        {
            if (rightDifficultyInputPressed) {
                rightDifficultyInputImage.transform.localScale = new Vector3(t.Value, t.Value, t.Value);
            } else {
                leftDifficultyInputImage.transform.localScale = new Vector3(t.Value, t.Value, t.Value);
            }
        };

        updateDifficultyInputsScaleEnd = (t) =>
        {
            if (rightDifficultyInputPressed) {
                inputsDifficultyScaleTween = FloatTween.Create("TweenInputsDifficultydownScale", 1.15f, 1f, inputsDifficultyTime, EasingFunction.Ease.EaseInOutCubic, updateDifficultyInputsScale);
            } else {
                inputsDifficultyScaleTween = FloatTween.Create("TweenInputsDifficultydownScale", 1.15f, 1f, inputsDifficultyTime, EasingFunction.Ease.EaseInOutCubic, updateDifficultyInputsScale);
            }
        };

        updateCountDownSprite = (t) =>
        {
            // We will ajust the tween scale function and countdown sprite at each step.
            switch (countDownStatus) {
                case 3:
                    countDownImage.sprite = num2Sprite;
                    countdownScaleTween = FloatTween.Create("TweenCountdownScale", 1.3f, 1.15f, countDownTime, EasingFunction.Ease.EaseInOutCubic, updateCountDownScale)
                        .OnComplete(updateCountDownSprite);
                    break;
                case 2:
                    countDownImage.sprite = num1Sprite;
                    countdownScaleTween = FloatTween.Create("TweenCountdownScale", 1.15f, 1f, countDownTime, EasingFunction.Ease.EaseInOutCubic, updateCountDownScale)
                        .OnComplete(updateCountDownSprite);
                    break;
                // At the last step, the behavirous will depend of the current game status.
                case 1:
                    inCountdown = false;
                    countDown.SetActive(false);

                    if (tileManager.GetGameStatus() == TileManager.GameStatus.PAUSE) {
                        tileManager.ResumeGame();
                    } else if (tileManager.GetGameStatus() == TileManager.GameStatus.SETTINGS || tileManager.GetGameStatus() == TileManager.GameStatus.END) {
                        tileManager.StartGame();
                    }
                    break;
            }
            countDownStatus--;
        };
    }

    // Called when we press the confirm button (space)
    public void ConfirmInputDetected(InputAction.CallbackContext context) {
        if (context.started) {
            if (!inCountdown) {
                // Inputs when the game is paused.
                if (tileManager.GetGameStatus() == TileManager.GameStatus.PAUSE) {
                    // We update the button sprite when the player press space.
                    inPauseKeyPressing = true;
                    if (!pauseMenuIndex) {
                        pauseResumeButton.sprite = pressedButton;
                    } else {
                        pauseQuitButton.sprite = pressedButton;
                    }
                } else if (tileManager.GetGameStatus() == TileManager.GameStatus.END) {
                    // We update the button sprite when the player press space.
                    inEndKeyPressing = true;
                    if (!endMenuIndex) {
                        endRestartButton.sprite = pressedButton;
                    } else {
                        endSettingsButton.sprite = pressedButton;
                    }
                }
            }
        } else if (context.canceled) {
            if (!inCountdown) {
                // Inputs when the game is paused.
                if (tileManager.GetGameStatus() == TileManager.GameStatus.PAUSE) {
                    // Execute the button action when the player release space.
                    if (!pauseMenuIndex) {
                        ResumeUI();
                    } else {
                        Application.Quit();
                    }
                } else if (tileManager.GetGameStatus() == TileManager.GameStatus.END) {
                    // Execute the button action when the player release space.
                    HideEndMenu();
                    if (!endMenuIndex) {
                        StartCountdown();
                    } else {
                        tileManager.SetStartStatus();
                        DisplaySettingsMenu();
                    }
                }
            }
        } 
        if (context.performed) {
            if (tileManager.GetGameStatus() == TileManager.GameStatus.SETTINGS) {
                // If the player wnats to start, we launch the countdown and hide the start menu.
                StartCountdown();
                HideSettingsMenu();
            }
        }
    }

    // Called when we press the Cancel button (escape)
    public void CancelInputDetected(InputAction.CallbackContext context) {
        if (!context.performed) {
            return;
        }
        else {
            if (!inCountdown) {
                // Press Escape to pause/unpause the game
                if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
                    PauseUI();
                } else if (tileManager.GetGameStatus() == TileManager.GameStatus.PAUSE) {
                    ResumeUI();
                } else if (tileManager.GetGameStatus() == TileManager.GameStatus.END || tileManager.GetGameStatus() == TileManager.GameStatus.SETTINGS) {
                    Application.Quit();
                }
            }
        }
    }

    // Called when we press the up arrow button (tap or hold)
    public void UpInputDetected(InputAction.CallbackContext context) {
        if (!context.performed) {
            return;
        }
        else {
            if (!inCountdown) {
                // Inputs when the game is paused.
                if (tileManager.GetGameStatus() == TileManager.GameStatus.PAUSE) {
                    // Switch the currently selected button with the right and left arrows only if the player didn't pressed any button yet.
                    if (!inPauseKeyPressing) {
                        pauseMenuIndex = !pauseMenuIndex;
                        ChangePauseMenuSprite();
                    }
                } else if (tileManager.GetGameStatus() == TileManager.GameStatus.END) { // Inputs when the game is ending.
                    // Switch the currently selected button with the right and left arrows only if the player didn't pressed any button yet.
                    if (!inEndKeyPressing) {
                        endMenuIndex = !endMenuIndex;
                        ChangeEndMenuSprite();
                    }
                }
            }
        }
    }

    // Called when we press the right arrow button (tap or hold)
    public void RightInputDetected(InputAction.CallbackContext context) {
        if (!context.performed) {
            return;
        }
        else {
            if (!inCountdown) {
                // Inputs when the game is starting.
                if (tileManager.GetGameStatus() == TileManager.GameStatus.SETTINGS) {
                    SwitchDifficultySprite(true);
                    if (!inputsDifficultyScaleTween.IsRunning) {
                        rightDifficultyInputPressed = true;
                        inputsDifficultyScaleTween = FloatTween.Create("TweenInputsDifficultydownScale", 1f, 1.15f, inputsDifficultyTime, EasingFunction.Ease.EaseInOutCubic, updateDifficultyInputsScale)
                            .OnComplete(updateDifficultyInputsScaleEnd);
                    }
                }
            }
        }
    }

    // Called when we press the down arrow button (tap or hold)
    public void DownInputDetected(InputAction.CallbackContext context) {
        if (!context.performed) {
            return;
        }
        else {
            if (!inCountdown) {
                // Inputs when the game is paused.
                if (tileManager.GetGameStatus() == TileManager.GameStatus.PAUSE) {
                    // Switch the currently selected button with the right and left arrows only if the player didn't pressed any button yet.
                    if (!inPauseKeyPressing) {
                        pauseMenuIndex = !pauseMenuIndex;
                        ChangePauseMenuSprite();
                    }
                } else if (tileManager.GetGameStatus() == TileManager.GameStatus.END) { // Inputs when the game is ending.
                    // Switch the currently selected button with the right and left arrows only if the player didn't pressed any button yet.
                    if (!inEndKeyPressing) {
                        endMenuIndex = !endMenuIndex;
                        ChangeEndMenuSprite();
                    }
                }
            }
        }
    }

    // Called when we press the left arrow button (tap or hold)
    public void LeftInputDetected(InputAction.CallbackContext context) {
        if (!context.performed) {
            return;
        }
        else {
            if (!inCountdown) {
                // Inputs when the game is starting.
                if (tileManager.GetGameStatus() == TileManager.GameStatus.SETTINGS) {
                    SwitchDifficultySprite(false);
                    if (!inputsDifficultyScaleTween.IsRunning) {
                        rightDifficultyInputPressed = false;
                        inputsDifficultyScaleTween = FloatTween.Create("TweenInputsDifficultydownScale", 1f, 1.15f, inputsDifficultyTime, EasingFunction.Ease.EaseInOutCubic, updateDifficultyInputsScale)
                            .OnComplete(updateDifficultyInputsScaleEnd);
                    }
                }
            }
        }
    }

    public void UpdateTimerValue(double newTimerValue) {
        timerText.text = newTimerValue.ToString();
    }

    // We update the difficulty sprite depending on the current displayed sprite.
    // Using an index is way better, but it will work for V1 of the game, will be updated later with mode difficulty options/settings.
    private void SwitchDifficultySprite(bool up) {
        if (up) {
            switch(difficultyImage.sprite.name) {
                case "easy_text":
                    difficultyImage.sprite = difficultyMedium;
                    tileManager.SetDifficulty(2);
                    break;
                case "medium_text":
                    difficultyImage.sprite = difficultyHard;
                    tileManager.SetDifficulty(3);
                    break;
                case "hard_text":
                    difficultyImage.sprite = difficultyEasy;
                    tileManager.SetDifficulty(1);
                    break;
            }
        } else {
            switch(difficultyImage.sprite.name) {
                case "easy_text":
                    difficultyImage.sprite = difficultyHard;
                    tileManager.SetDifficulty(3);
                    break;
                case "medium_text":
                    difficultyImage.sprite = difficultyEasy;
                    tileManager.SetDifficulty(1);
                    break;
                case "hard_text":
                    difficultyImage.sprite = difficultyMedium;
                    tileManager.SetDifficulty(2);
                    break;
            }
        }
        difficultyImage.SetNativeSize();
    }

    // We update the buttons sprites (selected/unpressed).
    // Not the best solution to use a bool there, but there won't be any more buttons to that's fine.
    private void ChangePauseMenuSprite() {
        if (!pauseMenuIndex) {
            pauseResumeButton.sprite = selectedButton;
            pauseQuitButton.sprite = unpressedButton;
        } else {
            pauseQuitButton.sprite = selectedButton;
            pauseResumeButton.sprite = unpressedButton;
        }
    }

    // Same as ChangePauseMenuSprite for the end menu.
    private void ChangeEndMenuSprite() {
        if (!endMenuIndex) {
            endRestartButton.sprite = selectedButton;
            endSettingsButton.sprite = unpressedButton;
        } else {
            endSettingsButton.sprite = selectedButton;
            endRestartButton.sprite = unpressedButton;
        }
    }

    // Setting up all variables for the pause menu.
    private void PauseUI() {
        pauseMenu.SetActive(true);
        inPauseKeyPressing = false;
        pauseMenuIndex = false;
        pauseResumeButton.sprite = selectedButton; 
        pauseQuitButton.sprite = unpressedButton;
        tileManager.PauseGame();
    }

    private void ResumeUI() {
        pauseMenu.SetActive(false);
        StartCountdown();
    }

    // Setting up all variables for the end menu.
    public void DisplayEndMenu(float finalScore) {
        endMenu.SetActive(true);
        scoreText.text = finalScore.ToString();
        endMenuIndex = false;
        inEndKeyPressing = false;
        endRestartButton.sprite = selectedButton; 
        endSettingsButton.sprite = unpressedButton;
    }

    private void HideEndMenu() {
        endMenu.SetActive(false);
    }

    private void DisplaySettingsMenu() {
        settingsMenu.SetActive(true);
    }

    private void HideSettingsMenu() {
        settingsMenu.SetActive(false);
    }

    // Setting up all variables for the countdown.
    public void StartCountdown() {
        inCountdown = true;
        countDown.SetActive(true);
        countDownStatus = 3;
        countDownImage.sprite = num3Sprite;
        countdownScaleTween = FloatTween.Create("TweenCountdownScale", 1.45f, 1.3f, countDownTime, EasingFunction.Ease.EaseInOutCubic, updateCountDownScale)
            .OnComplete(updateCountDownSprite);
    }
}
