using UnityEngine;
using System;
using System.Collections.Generic;

// Modified version of plugin Tween from Digital Ruby (https://assetstore.unity.com/packages/tools/animation/tween-55983?aid=1011lGnL&utm_source=aff#content).
// Modifications made by Simon Colin (you can contact him there @SimonColin7).

namespace Tween
{
    public class TweenStore : MonoBehaviour
    {
        private static Dictionary<string, ITween> dictionary = new Dictionary<string, ITween>();
        private static List<string> toDelete = new List<string>();
        private static Dictionary<string, ITween> toStore = new Dictionary<string, ITween>();
        
        public static void Store(string k, ITween t)
        {
            toStore.Add(k, t);
        }

        public static void Delete(string k)
        {
            toDelete.Add(k);
            dictionary.TryGetValue(k, out ITween t);
            if(t != null)
            {
                t.Pause();
            }
        }

        public static void Delete(ITween t)
        {
            toDelete.Add(t.Key);
            t.Pause();
        }
    
        private void Update()
        {
            float elapsedTime = Time.deltaTime;
            foreach(ITween t in dictionary.Values)
            {
                t.Update(elapsedTime);
            }

            foreach(string s in toDelete)
            {
                dictionary.Remove(s);
            }
            toDelete.Clear();

            foreach(string s in toStore.Keys)
            {
                if(dictionary.ContainsKey(s))
                {
                    dictionary.Remove(s);
                }
                toStore.TryGetValue(s, out ITween t);
                if(t != null) {
                    dictionary.Add(s, t);
                }
            }
            toStore.Clear();
        }
    }

    public interface ITween {
        string Key { get; }
        void Pause();
        void Resume();
        void Restart();
        void Finish();
        void Update(float elapsedTime);
    }

    public class Tween<T> : ITween
    {
        private GameObject root;
        private GameObject toDestroy;

        protected bool running = false;
        protected string key;
        protected float duration = 0f;
        protected EasingFunction.Function easeFunc = null;
        protected System.Action<Tween<T>> func = null;

        protected System.Action<Tween<T>> onComplete = null;
        protected ITween nextTween = null;

        private readonly Func<Tween<T>, T, T, float, T> lerpFunc;

        protected float curDuration = 0f;
        protected float progress = 0f;

        public string Key { get => key; }
        public bool IsRunning { get => running; }
        public float Duration { get => duration; }
        public float Progress { get => progress; }

        private T start;
        private T end;
        private T value;

        public T Start { get => start; }
        public T End { get => end; }
        public T Value { get => value; }

        public Tween(Func<Tween<T>, T, T, float, T> lerpFunc)
        {
            this.lerpFunc = lerpFunc;
        }

        public Tween<T> Setup(T start, T end, float duration, EasingFunction.Ease ease, System.Action<Tween<T>> func)
        {
            Initialize();
            this.start = start;
            this.end = end;
            this.duration = duration;
            this.easeFunc = EasingFunction.GetEasingFunction(ease);
            this.func = func;
            Restart();
            return this;
        }

        private void Build(string key, T start, T end, float duration, EasingFunction.Ease ease, System.Action<Tween<T>> func)
        {
            this.key = key;
            TweenStore.Store(key, this);
            this.Setup(start, end, duration, ease, func);
        }

        public static void Delete(string key)
        {
            TweenStore.Delete(key);
        }

        private void SetKey(string k)
        {
            this.key = k;
        }

        public void Initialize()
        {
            if (root == null && Application.isPlaying)
            {
                root = GameObject.Find("DigitalRubyTween");
                if (root == null || root.GetComponent<TweenStore>() == null)
                {
                    if (root != null)
                    {
                        toDestroy = root;
                    }
                    root = new GameObject { name = "DigitalRubyTween", hideFlags = HideFlags.HideAndDontSave };
                    root.AddComponent<TweenStore>().hideFlags = HideFlags.HideAndDontSave;
                }
                if (Application.isPlaying)
                {
                    GameObject.DontDestroyOnLoad(root);
                }
            }
        }

        public void Pause()
        {
            running = false;
        }

        public void Resume()
        {
            running = true;
        }

        public void Restart()
        {
            curDuration = 0f;
            progress = 0f;
            Resume();
        }

        public void Finish()
        {
            progress = 1f;
            value = end;
            running = false;
            TweenStore.Delete(key);
            if(nextTween != null)
            {
                nextTween.Restart();
                TweenStore.Store(key, nextTween);
            }
            if(onComplete != null)
            {
                onComplete(this);
            }
        }

        public Tween<Tnew> ContinueWith<Tnew>(Tween<Tnew> t, bool launchIfFinished = false) where Tnew : struct {
            nextTween = t;
            t.SetKey(key);
            t.Pause();
            if(launchIfFinished && progress >= 1)
            {
                nextTween.Restart();
                TweenStore.Store(key, nextTween);
            }
            return t;
        }

        public Tween<T> OnComplete(System.Action<Tween<T>> onComplete, bool launchIfFinished = false) {
            this.onComplete = onComplete;
            if(launchIfFinished && progress >= 1)
            {
                this.onComplete(this);
            }
            return this;
        }

        public void Update(float elapsedTime)
        {
            if (toDestroy != null)
            {
                GameObject.Destroy(toDestroy);
                toDestroy = null;
            }
            if(running)
            {
                curDuration += elapsedTime;
                if(duration >= curDuration)
                {
                    progress = curDuration / duration;
                    value = lerpFunc(this, start, end, easeFunc(0, 1, progress));
                    func(this);
                }
                else
                {
                    Finish();
                }
            }
        }

        public static Vector3Tween Create(string key, Vector3 start, Vector3 end, float duration, EasingFunction.Ease ease, System.Action<Tween<Vector3>> func)
        {
            Vector3Tween t = new Vector3Tween();
            t.Build(key, start, end, duration, ease, func);
            return t;
        }

        public static Vector2Tween Create(string key, Vector2 start, Vector2 end, float duration, EasingFunction.Ease ease, System.Action<Tween<Vector2>> func)
        {
            Vector2Tween t = new Vector2Tween();
            t.Build(key, start, end, duration, ease, func);
            return t;
        }

        public static FloatTween Create(string key, float start, float end, float duration, EasingFunction.Ease ease, System.Action<Tween<float>> func)
        {
            FloatTween t = new FloatTween();
            t.Build(key, start, end, duration, ease, func);
            return t;
        }

        public static ColorTween Create(string key, Color start, Color end, float duration, EasingFunction.Ease ease, System.Action<Tween<Color>> func)
        {
            ColorTween t = new ColorTween();
            t.Build(key, start, end, duration, ease, func);
            return t;
        }

        public static QuaternionTween Create(string key, Quaternion start, Quaternion end, float duration, EasingFunction.Ease ease, System.Action<Tween<Quaternion>> func)
        {
            QuaternionTween t = new QuaternionTween();
            t.Build(key, start, end, duration, ease, func);
            return t;
        }
    }

    public class Vector3Tween : Tween<Vector3>
    {
        private static Vector3 LerpVector3(Tween<Vector3> t, Vector3 start, Vector3 end, float progress) { return Vector3.Lerp(start, end, progress); }
        private static readonly Func<Tween<Vector3>, Vector3, Vector3, float, Vector3> LerpFunc = LerpVector3;
        public Vector3Tween() : base(LerpFunc) { }
    }

    public class Vector2Tween : Tween<Vector2>
    {
        private static Vector2 LerpVector2(Tween<Vector2> t, Vector2 start, Vector2 end, float progress) { return Vector2.Lerp(start, end, progress); }
        private static readonly Func<Tween<Vector2>, Vector2, Vector2, float, Vector2> LerpFunc = LerpVector2;
        public Vector2Tween() : base(LerpFunc) { }
    }

    public class FloatTween : Tween<float>
    {
        private static float Lerpfloat(Tween<float> t, float start, float end, float progress) { return start + (end - start) * progress; }
        private static readonly Func<Tween<float>, float, float, float, float> LerpFunc = Lerpfloat;
        public FloatTween() : base(LerpFunc) { }
    }

    public class ColorTween : Tween<Color>
    {
        private static Color LerpColor(Tween<Color> t, Color start, Color end, float progress) { return Color.Lerp(start, end, progress); }
        private static readonly Func<Tween<Color>, Color, Color, float, Color> LerpFunc = LerpColor;
        public ColorTween() : base(LerpFunc) { }
    }

    public class QuaternionTween : Tween<Quaternion>
    {
        private static Quaternion LerpQuaternion(Tween<Quaternion> t, Quaternion start, Quaternion end, float progress) { return Quaternion.Lerp(start, end, progress); }
        private static readonly Func<Tween<Quaternion>, Quaternion, Quaternion, float, Quaternion> LerpFunc = LerpQuaternion;
        public QuaternionTween() : base(LerpFunc) { }
    }
}