﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using Tween;

// Class used for player movements and to get the player's current 'grid' position (updated everytime the player moves).
public class PlayerController : MonoBehaviour
{
    // The duration of the player's movement used for the postion tweening.
    private float movementDuration = 0.5f;

    private int inputType;
    private int inputDirection;

    [SerializeField] private UIManager uiManager;
    // We store a reference of the tileManager script.
    [SerializeField] private TileManager tileManager;
    // Reference to the Animator.
    private Animator animator;
    // Tween function for the X player's position.
    private System.Action<Tween<float>> updatePlayerXPosition;
    // Tween function for the X player's position.
    private System.Action<Tween<float>> updatePlayerYPosition;
    // Tween function to notify the tileManager that the player moved (the position tween finished).
    private System.Action<Tween<float>> updateTileManagerNextMovement;
    // Tween used to move the player.
    private Tween<float> playerPositionTween;
    // Vector2 that stores the current player's position in the grid.
    private Vector2 playerPositionGrid;


    private void Start() {
        animator = GetComponent<Animator>();
        animator.Play("no_anim");

        updatePlayerXPosition = (t) =>
        {
            transform.localPosition = new Vector3(t.Value, transform.localPosition.y, transform.localPosition.z);
        };
        
        updatePlayerYPosition = (t) =>
        {
            transform.localPosition = new Vector3(transform.localPosition.x, t.Value, transform.localPosition.z);
        };

        updateTileManagerNextMovement = (t) =>
        {
            tileManager.NextPlayerMovement();
        };

        // We need to create the tween so we can check if the tween is running in the update method.
        playerPositionTween = FloatTween.Create("TweenPlayerPosition", transform.localPosition.x, transform.localPosition.x, 0.001f, EasingFunction.Ease.EaseInOutCubic, updatePlayerXPosition);
        // We set the player's initial position
        playerPositionGrid = new Vector2(0f, 0f);
    }

    public void UpInputDetected(InputAction.CallbackContext context) {
        if (!context.performed)
            return;
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
            // We check if the player is already moving before making any change.
            if (!playerPositionTween.IsRunning) {
                inputDirection = 0;
                inputType = tileManager.GetInputType(ref inputDirection);

                if (context.interaction is MultiTapInteraction) {
                    if (inputType == 1) {
                        HandlePlayerMovement(inputDirection); 
                    }
                }
                if (context.interaction is HoldInteraction) {
                    if (inputType == 2) {
                        HandlePlayerMovement(inputDirection);
                    }
                }
            }
        }
    }

    public void UpTapInputDetected(InputAction.CallbackContext context) {
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
            if (context.started) {
                // We check if the player is already moving before making any change.
                if (!playerPositionTween.IsRunning) {
                    inputDirection = 0;
                    inputType = tileManager.GetInputType(ref inputDirection);

                    if (inputType == 0 || inputType == 3) {
                        HandlePlayerMovement(inputDirection);
                    }
                } 
            }
        } 
    }

    public void RightInputDetected(InputAction.CallbackContext context) {
        if (!context.performed)
            return;
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
            // We check if the player is already moving before making any change.
            if (!playerPositionTween.IsRunning) {
                inputDirection = 1;
                inputType = tileManager.GetInputType(ref inputDirection);

                if (context.interaction is MultiTapInteraction) {
                    if (inputType == 1) {
                        HandlePlayerMovement(inputDirection); 
                    }
                }
                if (context.interaction is HoldInteraction) {
                    if (inputType == 2) {
                        HandlePlayerMovement(inputDirection);
                    }
                }
            }
        }
    }

    public void RightTapInputDetected(InputAction.CallbackContext context) {
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
            if (context.started) {
                // We check if the player is already moving before making any change.
                if (!playerPositionTween.IsRunning) {
                    inputDirection = 1;
                    inputType = tileManager.GetInputType(ref inputDirection);

                    if (inputType == 0 || inputType == 3) {
                        HandlePlayerMovement(inputDirection);
                    }
                }
            }    
        }
    }

    public void DownInputDetected(InputAction.CallbackContext context) {
        if (!context.performed)
            return;
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
            // We check if the player is already moving before making any change.
            if (!playerPositionTween.IsRunning) {
                inputDirection = 2;
                inputType = tileManager.GetInputType(ref inputDirection);

                if (context.interaction is MultiTapInteraction) {
                    if (inputType == 1) {
                        HandlePlayerMovement(inputDirection); 
                    }
                }
                if (context.interaction is HoldInteraction) {
                    if (inputType == 2) {
                        HandlePlayerMovement(inputDirection);
                    }
                }
            }
        }
    }

    public void DownTapInputDetected(InputAction.CallbackContext context) {
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
            if (context.started) {
                // We check if the player is already moving before making any change.
                if (!playerPositionTween.IsRunning) {
                    inputDirection = 2;
                    inputType = tileManager.GetInputType(ref inputDirection);

                    if (inputType == 0 || inputType == 3) {
                        HandlePlayerMovement(inputDirection);
                    }
                }
            }    
        }
    }

    public void LeftInputDetected(InputAction.CallbackContext context) {
        if (!context.performed)
            return;
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
            // We check if the player is already moving before making any change.
            if (!playerPositionTween.IsRunning) {
                inputDirection = 3;
                inputType = tileManager.GetInputType(ref inputDirection);

                if (context.interaction is MultiTapInteraction) {
                    if (inputType == 1) {
                        HandlePlayerMovement(inputDirection); 
                    }
                }
                if (context.interaction is HoldInteraction) {
                    if (inputType == 2) {
                        HandlePlayerMovement(inputDirection);
                    }
                }
            }
        }
    }

    public void LeftTapInputDetected(InputAction.CallbackContext context) {
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PLAY) {
            if (context.started) {
                // We check if the player is already moving before making any change.
                if (!playerPositionTween.IsRunning) {
                    inputDirection = 3;
                    inputType = tileManager.GetInputType(ref inputDirection);

                    if (inputType == 0 || inputType == 3) {
                        HandlePlayerMovement(inputDirection);
                    }
                }
            }
        }
    }

    public void PauseTween() {
        if (playerPositionTween != null && playerPositionTween.IsRunning)
            playerPositionTween.Pause();
    }

    public void ResumeTween() {
        if (playerPositionTween != null && !playerPositionTween.IsRunning && playerPositionTween.Progress < 1)
            playerPositionTween.Resume();
    }

    public void StartAnimator() {
        animator.Play("player_idle");
    }

    public void StopAnimator() {
        animator.Play("no_anim");
    }

    public void PauseAnimator() {
        animator.speed = 0;
    }
    public void ResumeAnimator() {
        animator.speed = 1;
    }

    private void HandlePlayerMovement(int movementDirection) {
        switch (movementDirection) {
            case 0:
                // Update the player's position in grid.
                playerPositionGrid.y += 2f;

                // Create the tween that will move the player.
                // On Complete, notice the tile manager that the movement is complete.
                playerPositionTween = FloatTween.Create("TweenPlayerPosition", transform.localPosition.y, transform.localPosition.y + 2f, movementDuration, EasingFunction.Ease.EaseInOutCubic, updatePlayerYPosition)
                    .OnComplete(updateTileManagerNextMovement);

                // Start movement animation.
                animator.Play("player_move");
                
                // Spawn a line of tiles on the left border of the screen.
                tileManager.SpawnLineUp();
                break;
            case 1:
                playerPositionGrid.x += 2f;

                playerPositionTween = FloatTween.Create("TweenPlayerPosition", transform.localPosition.x, transform.localPosition.x + 2f, movementDuration, EasingFunction.Ease.EaseInOutCubic, updatePlayerXPosition)
                    .OnComplete(updateTileManagerNextMovement);

                animator.Play("player_move");

                tileManager.SpawnLineRight();
                break;
            case 2:
                playerPositionGrid.y -= 2f;

                playerPositionTween = FloatTween.Create("TweenPlayerPosition", transform.localPosition.y, transform.localPosition.y - 2f, movementDuration, EasingFunction.Ease.EaseInOutCubic, updatePlayerYPosition)
                    .OnComplete(updateTileManagerNextMovement);

                animator.Play("player_move");
                
                tileManager.SpawnLineDown();
                break;
            case 3:
                playerPositionGrid.x -= 2f;

                playerPositionTween = FloatTween.Create("TweenPlayerPosition", transform.localPosition.x, transform.localPosition.x - 2f, movementDuration, EasingFunction.Ease.EaseInOutCubic, updatePlayerXPosition)
                    .OnComplete(updateTileManagerNextMovement);

                animator.Play("player_move");
                
                tileManager.SpawnLineLeft();
                break;
        }
        // We notice the tile manager that we started a movement.
        tileManager.StartingPlayerMovement(inputType);
    }

    

    public Vector2 GetPlayerPosition() {
        return playerPositionGrid;
    }
}
