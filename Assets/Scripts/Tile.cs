﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tween;

// Class used to handle all the tile instances modifications.
public class Tile : MonoBehaviour
{
    // Bool needed to avoid the instant despawn of the tile right after spawning.
    private bool justSpawned;
    // Bool that we switch everytime the scale input tween ends to start the next one. 
    private bool inputScaleTweenStart;
    // Bool used to end the scale input tween after it disappeared.
    private bool inputScaleTweenStop;
    // Bool used to know if the tile has receivec an input or not.
    private bool hasAnInput;

    private int inputType;
    private int inputDirection; 
    private int originalDirection;

    // Duration of the scale input tween.
    private float inputScaleTweenDuration = 0.20f;
    // Duration of the transparency input tween.
    private float inputTransparencyTweenDuration = 0.15f;


    // Reference to the tileManager.
    private TileManager tileManager;

    // Reference to the SpriteRenderer of the input.
    [SerializeField] private SpriteRenderer inputRenderer;
    // Reference to the SpriteRenderer of the tile color.
    [SerializeField] private SpriteRenderer blocColorRenderer;
    // Red custom color.
    private Color redColor;
    // Green custom color.
    private Color greenColor;
    // Current color of the tile.
    private Color currentColor = Color.white;
    // Reference to the main camera.
    private Camera mainCamera;

    // Tween function for the tile input scale.
    private System.Action<Tween<float>> updateTileInputScale;
    // Tween function when the tile input scale end.
    private System.Action<Tween<float>> updateTileInputScaleEnd;
    // Tween function for the tile input transparency.
    private System.Action<Tween<float>> updateTileInputTransparency;
    // Tween function when the tile input transparency end.
    private System.Action<Tween<float>> updateTileInputTransparencyEnd;

    // Tween used to scale the input sprite up and down.
    private Tween<float> tileInputScaleTween;
    // Tween used to turn down the transparency of the input sprite once the player moved.
    private Tween<float> tileInputTransparencyTween;

    private void Start() {
        // We search for the instance of TilerManager.
        tileManager = GameObject.Find("Background Tiles").GetComponent<TileManager>();

        hasAnInput = false;

        updateTileInputScale = (t) =>
        {
            inputRenderer.transform.localScale = new Vector3(t.Value, t.Value, t.Value);
        };

        updateTileInputScaleEnd = (t) =>
        {
            if (!inputScaleTweenStop) {
                if (inputScaleTweenStart)
                    tileInputScaleTween = FloatTween.Create("TweenTileInputScale" + transform.GetInstanceID(), 1.1f, 1f, inputScaleTweenDuration, EasingFunction.Ease.EaseInOutCubic, updateTileInputScale).OnComplete(updateTileInputScaleEnd);
                else
                    tileInputScaleTween = FloatTween.Create("TweenTileInputScale" + transform.GetInstanceID(), 1f, 1.1f, inputScaleTweenDuration, EasingFunction.Ease.EaseInOutCubic, updateTileInputScale).OnComplete(updateTileInputScaleEnd);
                inputScaleTweenStart = !inputScaleTweenStart;
            }
        };

        updateTileInputTransparency = (t) =>
        {
            inputRenderer.color = new Color(1, 1, 1, t.Value);
        };
        
        updateTileInputTransparencyEnd = (t) =>
        {
            inputScaleTweenStop = true;
            inputRenderer.sprite = null;
            hasAnInput = false;
        };

        // Setting up custom colors.
        redColor = new Color(0.5f, 0f, 0f);
        greenColor = new Color(0f, 0.5f, 0f);

        // We get the camera instance.
        mainCamera = Camera.main;
    }

    private void Update() {
        // We need to check the boolean to avoid the instant despawn of the tile right after spawning.
        if (!justSpawned && !IsTargetVisible(mainCamera, gameObject)) {
            StartCoroutine(WaitAndReturnTile());
        }
    }

    // Coroutine used to wait for a second and switch the bool status.
    IEnumerator WaitOneSec() {
        yield return new WaitForSeconds(1f);
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PAUSE) {
            StartCoroutine(WaitOneSec());
        } else {
            yield return new WaitForSeconds(1f);
            justSpawned = false;
        }
    }

    // Coroutine used to wait for a second and return the tile to the tile pool.
    IEnumerator WaitAndReturnTile() {
        yield return new WaitForSeconds(1f);
        if (tileManager.GetGameStatus() == TileManager.GameStatus.PAUSE) {
            StartCoroutine(WaitAndReturnTile());
        } else {
            yield return new WaitForSeconds(1f);
            tileManager.ReturnTile(gameObject);
        }
    }

    // Method to start the timer spawn and reset the bool (used after each line of tile spawn).
    public void ResetSpawnTimer() {
        justSpawned = true;
        StartCoroutine(WaitOneSec());
    }

    public void PauseTween() {
        if (tileInputScaleTween != null && tileInputScaleTween.IsRunning)
            tileInputScaleTween.Pause();
        if (tileInputTransparencyTween != null && tileInputTransparencyTween.IsRunning)
            tileInputTransparencyTween.Pause();
    }

    public void ResumeTween() {
        if (tileInputScaleTween != null && !tileInputScaleTween.IsRunning && tileInputScaleTween.Progress < 1)
            tileInputScaleTween.Resume();
        if (tileInputTransparencyTween != null && !tileInputTransparencyTween.IsRunning && tileInputTransparencyTween.Progress < 1)
            tileInputTransparencyTween.Resume();
    }

    // Method used to check if the object in parameter is in the camera view.
    // Extracted from an anwser from "oPless_" (https://answers.unity.com/questions/8003/how-can-i-know-if-a-gameobject-is-seen-by-a-partic.html).
    bool IsTargetVisible(Camera c, GameObject go) {
        var planes = GeometryUtility.CalculateFrustumPlanes(c);
        var point = go.transform.position;
        foreach (var plane in planes) {
            if (plane.GetDistanceToPoint(point) < 0)
                return false;
        }
        return true;
    }

    public Color GetCurrentColor() {
        return currentColor;
    }

    public void SetCurrentColor(int newColor) {
        switch (newColor) {
            case 0:
                if (redColor.a == 0)
                    currentColor = new Color(0.5f, 0f, 0f);
                else
                    currentColor = redColor; 
                break;
            case 1:
                if (greenColor.a == 0)
                    currentColor = new Color(0f, 0.5f, 0f);
                else
                    currentColor = greenColor; 
                break;
            case 2:
                currentColor = Color.white;
                break;
        }
        UpdateTileColor();
    }

    public void StartInputScaleTween() {
        inputScaleTweenStart = true;
        inputScaleTweenStop = false;
        tileInputScaleTween = FloatTween.Create("TweenTileInputScale" + transform.GetInstanceID(), 1f, 1.1f, inputScaleTweenDuration, EasingFunction.Ease.EaseInOutCubic, updateTileInputScale).OnComplete(updateTileInputScaleEnd);
    }

    public void StartInputTransparencyTween() {
        tileInputTransparencyTween = FloatTween.Create("TweenTileInputTransparency" + transform.GetInstanceID(), 1f, 0f, inputTransparencyTweenDuration, EasingFunction.Ease.EaseInOutCubic, updateTileInputTransparency).OnComplete(updateTileInputTransparencyEnd);
    }

    public void SetCurrentInput(Sprite inputSprite, int newInputDirection, int newInputType, int newOriginalDirection = -1) {
        hasAnInput = true;
        inputRenderer.sprite = inputSprite;
        inputRenderer.color = Color.white;
        inputDirection = newInputDirection;
        inputType = newInputType;
        originalDirection = newOriginalDirection;
    }

    public bool HasAnInput() {
        return hasAnInput;
    }

    public int GetInputDirection() {
        return inputDirection;
    }

    public int GetInputType() {
        return inputType;
    }

    public int GetOriginalInputDirection() {
        return originalDirection;
    }

    private void UpdateTileColor() {
        blocColorRenderer.color = currentColor;
    }
}
